package qa.java.sol.calculator;

import java.awt.*;
import java.awt.event.*;

public class AboutDialog extends Dialog
{

	public AboutDialog(Frame parent)
	{
		super(parent, "About Calculator", true);  // modal

		// Create a panel to hold text
		Panel textPanel = new Panel();
		textPanel.add(new Label("Calculator 2.0"));
		add("Center", textPanel);

		// Create a second panel for the OK button
		Panel buttonsPanel = new Panel();
		Button okButton = new Button("OK");

        okButton.addActionListener
		(
		    new ActionListener()
		    {
		        public void actionPerformed(ActionEvent evt)
		        {
		            dispose();
		        }
		    }
	    );


        addWindowListener
		(
		    new WindowAdapter()
		    {
		        public void windowClosing(WindowEvent evt)
		        {
		            dispose();
		        }
		    }
	    );

		buttonsPanel.add(okButton);
		add("South", buttonsPanel);

		setSize(200, 100);
	}



}