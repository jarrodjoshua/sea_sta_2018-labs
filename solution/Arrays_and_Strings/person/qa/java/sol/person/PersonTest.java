/* PersonTest is a Java application for testing the Person class */
package qa.java.sol.person;
import java.util.*;

public class PersonTest
{

	// Class variables to hold sample test values
	//
	private static String[] testNames = {"Trulli", "Baumgartner", "Montoya", 
					     "Sato", "Coulthard", "Barrichello",
					     "Schumacher", "Button", "Alonso",
					     "Fisichella", "Bruni", "Webber" };
	private static int[] testAges = {36, 26, 25, 29, 28, 31, 32, 37, 28, 30, 32, 32};


	//
	// The main() method is the entry point of this class
	//
	public static void main(String[] args)
	{
		// Declare a variable capable of referencing an array of persons
		//
		Person[] persons;

		// Create an array capable of referencing some number of persons
		//
		int count = testNames.length;
		persons = new Person[count];

		// Fill array with references to Person objects
		//
		for (int i = 0; i < persons.length; i++){
			persons[i] = new Person(testNames[i], testAges[i]);
		}

		// Print initial contents of array
		//
		System.out.println("\nThe original list of people is:\n");
		for (int i = 0; i < persons.length; i++) {
			System.out.println("  " + persons[i].getName() + "\t" + persons[i].getAge());
		}

		// Sort array so that names are in age order
		//
		Person.bSortByAge(persons);

		// Sort array so that names are in alphabetical order
		//Person.bSortByName(persons);

		 
		// Solution to optional practical at end of inner class lab
		// Creates an anonymous inner class implementing Comparator that sorts by name  
		/*
		Arrays.sort(persons, new Comparator<Person>() {
		
			public int compare(Person p1, Person p2) {
				// TODO Auto-generated method stub
				return p1.getName().compareTo(p2.getName());
			}
		
		});
		*/
	
		// Solution to last part of optional practical at end of inner class lab
		// Creates a static helper class in Person implementing 
		// Comparator<Person> that sorts by name
		 
		// Arrays.sort(persons, new Person.SortByName());
		

		// Print sorted contents of array
		//
		System.out.println("\nThe sorted list of people is:\n");
		for (int i = 0; i < persons.length; i++) {
			System.out.println("  " + persons[i].getName() + "\t" + persons[i].getAge());
		}

	}

}