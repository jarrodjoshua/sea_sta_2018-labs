/*
 * CustomerTest class
 *
 * CustomerTest is a Java application for testing the Customer class.
 *
 */
package qa.java.sol.customer1.part2;

public class CustomerTest
{
	//
	// The main() method is the entry point of this class
	//
	public static void main(String[] args)
	{
		//
		// Declare two references to Customer objects
		//
		Customer customer1, customer2;

		//
		// Create two new Customer objects
		//
	    customer1 = new Customer("Smith", 101);
        customer2 = new Customer("Jones", 102);

		// Display headings for Customers' details
		//
		System.out.println("\n" + "Customer" + "\t" + "A/C No." + "\n");

		// Display details of both Customers
		//
		// First attempt: access instance variables directly

		// Second attempt: use accessor methods
		//
		System.out.println(customer1.getName() + "\t\t" + customer1.getAccountNumber());
		System.out.println(customer2.getName() + "\t\t" + customer2.getAccountNumber());


		// Test setName mutator method
		customer1.setName("Bruce Banner");
		customer2.setName("Tony Stark");


		// Re-display details of both customers
		//
		System.out.println();
		System.out.println(customer1.getName() + "\t\t" + customer1.getAccountNumber());
		System.out.println(customer2.getName() + "\t\t" + customer2.getAccountNumber());

	}

}
