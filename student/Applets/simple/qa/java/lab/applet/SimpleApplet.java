package qa.java.lab.applet;
/*
 *
 * SimpleApplet
 *
 */

import java.applet.*;
import java.awt.*;

public class SimpleApplet {

//
// ToDo:
//
// Declare the class SimpleApplet which subclasses
// Applet.
//
// Declare a string to hold your message and
// initialise it with a default value.
//
// Override the paint() method and use the
// Graphics object's drawString() method to
// draw your string on the applet.
//
// Note that the coordinates for drawString() are
// for the bottom of the text, so the y-coordinate
// should be 10 or more for it to be properly
// visible
//

//
// ToDo:
//
// Once the basic applet works OK, add a line
// into your HTML file defining a parameter
// called MyMessage.
//
// Add code to your applet to check to see if
// this has been defined. If so, use it. If not,
// use the default message.
//

}