
$(document).ready(function(){
	
	// TO DO:
	// =====
	//
	// On loading, hide the inner menus - the <ul> elements
	// inside the outer <li> elements
	// Instead of just calling hide() on the elements, we suggest 
	// you chain the following click-handler after the hide(), to stop the click propagating
	// up to the parent elements. 
	
	// click(function(event){
	//	event.stopPropagation();
	//	});
	
	//(Alternatively, you could just make this 
	// a separate function call to happen on loading.) 
	//
	// If an event occurs on an inner element, it is passed to any event handler 
	// for that element; then, when the event handler function has finished executing, 
	// the event is passed up the DOM to the parent, and the parent's event handler is 
	// invoked (if one exists). This continues all the way up the DOM.
	//
	
	
	
	
	//the menu items expand into menus of their own...
	$("#menu li").mouseover(function(){

	
	// TO DO:
	// =====
	//
	// Define mouseover event handler to slide down the inner menus (hint: you might find the find() function useful!)
	//

	});
	$("#menu li").mouseout(function(){
	
	// TO DO:
	// =====
	//
	// Define mouseout event handler to slide up the inner menus (hint: you might find the find() function useful!)
	//
	});


	//handle tabs
	//initialise...set 1st tab to selected 
	// Note the use of the "selected" CSS class
	$("#tab_list li:first").addClass("selected");

	//initialise...hide all content except 1st para
	$("#content_div p:not(:first)").hide();
	
	//enable click-event handler for all tabs
	$("#tab_list li").click(function(){
		//deal with the tabs
		
	// TO DO:
	// =====
	//
	// 	First set all tabs to not selected...
	//  (Hint: check to see how we initialised the tabs so that the first was originally 
	//  selected when the page was loaded.)
	//
	//  Now set the clicked tab to selected 
	//
	//  At this point, the tabs are dealt with. Time to deal with the 
	//  paragraphs of content.
	// 
	//  Start by hiding all of the paragraphs containing content
	//
	// 	Now, if you check the HTML, you'll see that the href value in each hyperlink in the 
	// 	tab_list is identical to the id of the corresponding content paragraph.
	// 
	//	The following two lines of code, when uncommented and completed, 
	//  will cause content paragraph to appear. 

	// var selectlink =  . . . . . TO DO obtain the value of the href here, and assign to the variable selectLink
		//$(selectlink).fadeIn("fast");
	});
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//lighten the background colour of alternating columns in table
	//(this technique workds only if the number of columns is even)
	$("tr td:even").css("background-color", "#117711");
	//change the background colour of a row while it's being pointed at
	//note that the hover function combines mouseover and mouseout: it
	//therefore requires two functions as arguments, one for pointer moving onto
	//the element and one for leaving the element
	$("tbody tr").hover(function(){
		$(this).css("background-color", "#E05500");
	}, function(){
		$(this).css("background-color", "#004422");
	});

 	
});