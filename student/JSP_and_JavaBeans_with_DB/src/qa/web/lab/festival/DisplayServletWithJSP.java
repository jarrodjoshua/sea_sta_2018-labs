package qa.web.lab.festival;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 * @version 	1.0
 * @author Lewis
 */
public class DisplayServletWithJSP extends HttpServlet {
	private PrintWriter out;

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {

		doPost(req, resp);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
		throws ServletException, IOException {

		out = resp.getWriter();

		doData(req, resp);

	}

	public void doData(HttpServletRequest req, HttpServletResponse resp) {

		ServletContext ctx = getServletContext();
		RequestDispatcher rd = null;
		String pageName = null;

		/*
		 * QA To do: exercise 2 step 6
		 *
		 * Provide code to call the JSPs via a RequestDispatcher.
		 * If the number conversion fails, you should forward
		 * to nodata.jsp; if it succeeds, you should forward
		 * to results.jsp.
		 */

		int day = 0;
		String dayString = req.getParameter("day");
		try {
			day = Integer.parseInt(dayString);
		} catch (Exception e) {
			e.printStackTrace();
		}


	}




}
